var configs = require('../configs/database.js');
var Sequelize = require('sequelize');
var sequelize = new Sequelize(configs.dbname, configs.username, configs.password, 
	{
	  host: configs.host,
	  dialect: configs.dialect,
	  storage: configs.storage
	}
);
var images = sequelize.define('images', {
	path: Sequelize.TEXT,
	latitude: Sequelize.REAL,
	longitude: Sequelize.REAL,
  	date: Sequelize.DATE
}, {timestamps: false});

function getImage(imageId, callback) {
	images.findById(imageId, {
		attributes: ['path', 'latitude', 'longitude', 'date']})
		.then(function (image) {
	    	callback(image.get());
		}
	);
}
function updateImage(imageId, fields, callback) {
	images.update(fields, { where: { id : imageId }})
	.then(function (result) { 
		callback(result);
  	});
};
function addImage(path, latitude, longitude, date, callback) {
	var image = images.build({path: path, latitude: latitude, longitude: longitude, date: date});
	image.save().then(function (image) {
    	callback(image.get('id'));
	});
};
module.exports = {
	getImage: getImage,
	addImage: addImage,
	updateImage: updateImage
};