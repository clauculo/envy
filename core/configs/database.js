module.exports = {
	dbname: 'envy',
	username: 'root',
	password: 'root',
	port: 3306,
	host: '127.0.0.1',
	dialect: 'sqlite',
	storage: 'core/source/envy'
};
