var AWS = require('aws-sdk');
var config = require('../configs/storage.js');
var AWS_BUCKET = config.bucket;

AWS.config.update(config.common);

function upload(key, stream, callback) {
    var s3 = new AWS.S3();

    var params = {
        Key: key + '.jpg',
        Body: stream,
        Bucket: AWS_BUCKET,
        ACL: 'public-read'
    };

    s3.upload(params, function(err, data) {
        if (err) {
            console.log('Error uploading data: ', err);
            callback(err, false);
        } else {
            console.log('Successfully uploaded %s with id %s', key, AWS_BUCKET);
            callback(null, data);
        }
    });
}

exports.upload = upload;
