var imagesModel = require('../models/images.js');

var multer  = require('multer');
var ExifImage = require('exif').ExifImage;
var Promise = require('bluebird');
var aws_operations = require('./aws_operations');
var crypto = require('crypto');

var storage = multer.memoryStorage();
var upload = multer({ storage: storage }).single('userPhoto');

function uploadImage(req, res) {
    return new Promise(function(result, reject) {
        upload(req, res, function(err) {
            if(err) {
                reject("Error uploading file.\n" + err);
            }
            result(req.file.buffer);
        });
    });
}

function getExifData(imageBuffer) {
    return new Promise(function(result, reject) {
        try {
            new ExifImage({
                image : imageBuffer
            }, function (error, exifData) {
                if (error) {
                    reject(error);
                } else {
                    result(exifData);
                }
            });
        } catch (error) {
            reject(error);
        }
    });
}

function getImage(imageId, res) {
	var image = imagesModel.getImage(imageId, function(image){
		res.send(image);
	});
};

function handleImage(req, res) {
    uploadImage(req, res)
        .then(function(imageBuffer) {
            getExifData(imageBuffer)
                .then(function(data) {
                    uploadImageToAWS(imageBuffer)
                        .then(function(imageUrl) {
                            var date = data.exif.DateTimeOriginal;
                            date = (date != undefined) ? date.split(' ')[0] : null;
                            var lat = data.gps.GPSLatitude;
                            var lon = data.gps.GPSLongitude;

                            var latRef = lat || "N";
                            var lonRef = lon || "W";
                            lat = (lat[0] + lat[1]/60 + lat[2]/3600);
                            lon = (lon[0] + lon[1]/60 + lon[2]/3600);

                            console.log(data);
                            console.log(data.gps);
                            console.log(data.exif);
                            console.log(data.gps.GPSLatitude);
                            saveToDB(imageUrl, lat, lon, date)
                                .then(function(imageId) {
                                    res.json({
                                        imageUrl: imageUrl,
                                        imageId: imageId,
                                        latitude: lat,
                                        longitude: lon,
                                        date: date
                                    });
                                });
                        });
                });
        })
        .catch(function(error) {
            console.log(error);
            res.status(500).send('Error: ' + error);
        });
};

function uploadImageToAWS(buffer) {
    return new Promise(function(result, reject) {
        var current_date = (new Date()).valueOf().toString();
        var random = Math.random().toString();
        var hash = crypto.createHash('sha1').update(current_date + random).digest('hex').substring(0, 30);
        aws_operations.upload(hash, buffer, function(error, data) {
            if (error) {
                reject(error);
            }
            result(data.Location);
        });
    });
}

function saveToDB(imageUrl, latitude, longitude, date) {
    return new Promise(function(result, reject) {
        imagesModel.addImage(imageUrl, latitude, longitude, date, result);
    });
}

module.exports = {
    handleImage: handleImage,
    getImage: getImage
};
