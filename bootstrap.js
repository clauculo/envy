var images = require('./core/controllers/images');
var express = require('express');
var path = require('path');

var app = express();

var googleApiKey = 'AIzaSyBCHk670F9gxBg2sLbicrft-jkUTEAObKQ';
var geocoder = require('./node_modules/node-geocoder')('google', 'https', googleApiKey);

var googleImages = require('google-images');
var client = googleImages('014709217507635969940:jtjc9-gew78', googleApiKey);

app.use(express.static(__dirname + '/'));
var bodyParser = require('body-parser');
app.use(bodyParser.json({ limit: '5mb' }));

app.get('/city', function(req, res) {
    geocoder.reverse({lat: req.query.lat, lon: req.query.lon})
    .then(function(r) {
        res.send(r);
    })
    .catch(function(e) {
        res.send(false);
    });
});

app.get('/ip', function(req, res) {
    var ip = req.connection.remoteAddress;
    res.send(ip);
});

app.get('/searchgoogle', function(req, res) {
    client.search(req.query.q)
    .then(function(images){
        res.send(images);
    });
});

app.get('/', function(req,res) {
    res.sendFile(path.resolve(__dirname, 'index.html'));
});

app.get('/upload', function(req, res) {
    res.sendFile(path.resolve(__dirname, 'view/upload.html'));
});

app.get('/api/photo', function(req, res) {
    images.getImage(req.query.id, res);
});

app.post('/api/photo', images.handleImage);

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});
