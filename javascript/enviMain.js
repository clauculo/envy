var enviMain = (function (){
    var activePage = 'intro';

    var pageNames = {
        intro: 'intro-page',
        chooseType: 'choose-type-page',
        uploadPhoto: 'upload-photo-page',
        uploadData: 'upload-data-page',
        checkImageData: 'check-image-data',
        cardPage: 'card-page',
        chooseTemplatePage: 'choose-template-page'
    };

    function switchPage(name){
        var pages = $('.page');
        pages.hide();
        pages.filter('#' +name).show();
        $('.bread-crambs').toggle(name !== pageNames.intro);
        $('.bread-crambs li').removeClass('active');
        $('.bread-crambs li').filter('.' + name).addClass('active');
        window.location.hash = name;

        if (name === pageNames.checkImageData){

        }
    }

    function initPages(){
        var hash = window.location.hash;
        $('.page').hide();
        $('.bread-crambs').hide();
        if(hash == '') {
        	$('#' + pageNames.intro).show();
        } else {
        	hash = hash.split('/');
        	$(hash[0]).show();
        	if(hash[1] != undefined) {
        		$.getJSON("/api/photo?id=" + hash[1], function(response){
					if(hash[1] != undefined && hash[1] != '') {
                        var data = {
							imageUrl: response.path,
                            latitude: response.latitude,
                            longitude: response.longitude,
                            date: response.date
						};
                        setImageData(data);
                        document.getElementById('uploadButton').style.display = 'none';
                		document.getElementById('createCardButton').style.display = 'block';
                		enviMain.imageData = data;
                		enviMain.mapPreview = mapManager.getMapPreviewSrc(response.latitude, response.longitude, 400, 300);
                		cardModel.setImageData();
			        };
        		});
        	}
    	}
    }

    function initPageSwitchers(){
        $('#switchToChooseTypePage').on('click', function(){
            switchPage(pageNames.chooseType);
        });

        $('#switchToUploadButton').on('click', function(){
            switchPage(pageNames.uploadPhoto);
        });

        $('#upload-photo-input').on('change', function(){
            switchPage(pageNames.uploadData);
        });

        $('#createCardButton').on('click', function() {
            switchPage(pageNames.cardPage);
        });

        $('#switchToTemplate').on('click', function() {
            switchPage(pageNames.chooseTemplatePage);
        });

        $('#switchToIntro').on('click', function () {
            goToIntro();
        });

    }

    function goToIntro(){
        $('.page').hide();
        $('.bread-crambs').hide();
        $('#' + pageNames.intro).show();
        window.location.hash = "";
    }

    $(function(){
        initPageSwitchers();
        initPages();
    });

    return {
        switchPage: switchPage
    };
})();
