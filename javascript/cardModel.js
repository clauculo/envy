var cardModel = (function() {

    $(function() {
        var frontPage = $('#frontPage');
        var backPage = $('#backPage');
        $('#toFrontPage').on('click', function() {
            frontPage.css('display', 'block');
            backPage.css('display', 'none');
        });

        $('#toBackPage').on('click', function() {
            frontPage.css('display', 'none');
            backPage.css('display', 'block');
        });
    })


    var CARD_WIDTH = 428 * 1.5;
    var CARD_HEIGHT = 298 * 1.5;

    var _imageData = null;

    function setImageData() {
        _imageData = enviMain.imageData;
        _mapPreview = enviMain.mapPreview;
        addFrontPage(_imageData.imageUrl);
        addBackPage(_mapPreview);
    }

    function addFrontPage(url) {
        var canvas = new fabric.Canvas('canvasFront', {
            width: CARD_WIDTH,
            height: CARD_HEIGHT
        });

        var img = new Image();
        img.onload = function() {
            var parentWidth = parseFloat(canvas.width);
            var parentHeight = parseFloat(canvas.height);
            var imgSize = calculateAspectRatioFit(img.width, img.height, parentWidth, parentHeight);
            var image = new fabric.Image(img);
            image.left = (parentWidth - imgSize.width) / 2;
            image.top = (parentHeight - imgSize.height) / 2;
            image.width = imgSize.width;
            image.height = imgSize.height;
            canvas.add(image);
            canvas.renderAll();
        }
        img.src = url;
    }

    function addBackPage(mapPreview) {
        var canvas = new fabric.Canvas('canvasBack', {
            width: CARD_WIDTH,
            height: CARD_HEIGHT
        });

        var img = new Image();
        img.onload = function() {
            var image = new fabric.Image(img);
            canvas.add(image);
            canvas.renderAll();
        }
        img.src = mapPreview;

        var img2 = new Image();
        img2.onload = function() {
            var imgSize = calculateAspectRatioFit(img2.width, img2.height, 100, 100);
            var image = new fabric.Image(img2);
            image.left = 150;
            image.top = 100;
            image.width = imgSize.width;
            image.height = imgSize.height;
            canvas.add(image);
            canvas.renderAll();
        }
        var wetherSrc = '';
        switch (enviMain.weatherId) {
            case 800:
                wetherSrc = '/images/weather/sun.png';
                break;
            case 801:
            case 802:
            case 803:
                wetherSrc = '/images/weather/cloudy.png';
                break;
            default:
                wetherSrc = '/images/weather/cloudy.png';
                break;
        }
        img2.src = wetherSrc;

        canvas.setOverlayImage('/images/back-overlay.png', canvas.renderAll.bind(canvas), {
            // Needed to position overlayImage at 0/0
            originX: 'left',
            originY: 'top'
        });
    }

    function calculateAspectRatioFit(srcWidth, srcHeight, maxWidth, maxHeight) {
        var ratio = Math.max(maxWidth / srcWidth, maxHeight / srcHeight);
        var rtnWidth = srcWidth * ratio;
        var rtnHeight = srcHeight * ratio;
        return {
            width: rtnWidth,
            height: rtnHeight
        };
    }

    function setOverlayImage(url){
        canvas.setOverlayImage('url', canvas.renderAll.bind(canvas), {
            // Needed to position overlayImage at 0/0
            originX: 'left',
            originY: 'top'
        });

    }
    return {
        setImageData: setImageData,
        setOverlayImage: setOverlayImage
    }
})();
