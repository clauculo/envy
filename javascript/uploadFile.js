function handleUpload() {
    var form = document.getElementById('uploadForm');
    var fileSelect = document.getElementById('upload-photo-input');
    var uploadButton = document.getElementById('uploadButton');
    var createCardButton = document.getElementById('createCardButton');
    var uploadImagePage = document.getElementById('upload-photo-page');
    //event.preventDefault();

    uploadButton.innerHTML = 'Uploading...';

    var files = fileSelect.files;

    var formData = new FormData();
    formData.append('userPhoto', files[0]);
    $.ajax({
        url: '/api/photo',
        type: 'POST',
        data: formData,
        cache: false,
        processData: false, // Don't process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
        dataType: 'text',  // what to expect back from the PHP script, if anything
        success: function(response, textStatus, jqXHR){
            if (typeof response.error === 'undefined') {
                uploadImagePage.getElementsByClassName('choose-type-header')[0].style.display = 'none';
                imageData = JSON.parse(response);
                enviMain.imageData = imageData;
                window.location.hash = window.location.hash + '/' + imageData.imageId;
                if (!imageData.latitude) {
                    navigator.geolocation.getCurrentPosition(function(position) {
                        setImageData(position.coords);
                    });
                } else {
                    setImageData(imageData);
                }
            }
        },
        error: function(jqXHR, textStatus, errorThrown){
        }
    });
}
function setImageData(imageData) {
    var startLat = imageData.latitude;
    var startLon = imageData.longitude;
    var mapPreview = mapManager.getMapPreviewSrc(startLat, startLon, 400, 300);

    $.getJSON("city?lat=" + startLat + "&lon=" + startLon, function(response){
        if(response != false) {
            var city = response[0]['city'];
            //document.getElementById('city').innerHTML = city;
        }
    });
    $.getJSON("http://api.openweathermap.org/data/2.5/weather?lat=" + startLat + "&lon=" + startLon + "&APPID=dced74929a9efe0cb46aa51788801edf",function(weatherJSON){
        var temp = Math.round((weatherJSON.main.temp_max  - 273.15)*10, 2)/10;
        var location = weatherJSON.name;
        document.getElementById("city").value = location;
        document.getElementById("temperature").innerHTML = temp + 'c';
        document.getElementById("weather").innerHTML = weatherJSON.weather[0].description;
        document.getElementById("weather_icon").className = "owf owf-" + weatherJSON.weather[0].id + " owf-5x";
        console.log(weatherJSON.weather[0]);
        document.getElementById("results").style.display = 'inline-block';
        document.getElementById("mapPreview").src = mapPreview;
        enviMain.weatherId = weatherJSON.weather[0].id;
        enviMain.mapPreview = mapPreview;

        if (weatherJSON.weather[0].id == 800) {
            document.getElementById("weather_icon").style.color = '#fd0';
        }

        cardModel.setImageData();

        $.getJSON('searchgoogle?q=' + location, function(results){
            $.each(results, function(k, v){
                $('.gallery').append('<li class="gallery-item"><img src="' + v.url + '" /></li>');
            });
        });

        // var images = [
        //     'https://pixabay.com/static/uploads/photo/2015/06/03/14/24/ladybug-796481_960_720.jpg',
        //     'https://pixabay.com/static/uploads/photo/2015/10/20/21/01/vineyard-998487_960_720.jpg',
        //     'http://www.vanessadewson.com/wp-content/uploads//galleries/post-1747/full/%C2%A9%20Vanessa%20Dewson-Nature%20%20001.jpg',
        //     'https://pixabay.com/static/uploads/photo/2015/01/07/17/47/nature-591708_960_720.jpg',
        //     'http://www.publicdomainpictures.net/pictures/20000/nahled/green-nature.jpg',
        //     'http://www.publicdomainpictures.net/pictures/50000/nahled/green-nature-1372563855yEI.jpg',
        //     'https://upload.wikimedia.org/wikipedia/commons/6/62/Nature_in_febr..jpg',
        //     'http://www.publicdomainpictures.net/pictures/30000/nahled/beauty-of-nature-5.jpg',
        //     'https://upload.wikimedia.org/wikipedia/commons/2/26/Bracken_Nature_Reserve_Cape_Town_South_Africa.jpg',
        //     'https://pixabay.com/static/uploads/photo/2015/12/25/20/49/landscape-1107984_960_720.jpg'
        // ];
        //
        // $.each(images, function(k, v){
        //     $('.gallery ul').append('<li class="gallery-item"><img src="' + v + '" /></li>');
        // });
    });
}
