var mapManager = (function(){
    var map;

    function getMapPreviewSrc(lat, lng, width, height) {
        var width = width || 300;
        var height = height || 300;
        var lat = lat || -13.24;
        var lng = lng || 150.666;

        var staticMapUrl = "https://maps.googleapis.com/maps/api/staticmap";

        //Set the Google Map Center.
        staticMapUrl += "?center=" + lat + "," + lng;

        //Set the Google Map Size.
        staticMapUrl += "&size="+ width + "x" + height + "&scale=1";

        //Set the Google Map Type.
        staticMapUrl += "&maptype=satelite";

        //Set the Google Map Zoom.
        staticMapUrl += "&zoom=" + 8;

        //Loop and add Markers.
        staticMapUrl += "&markers=" + lat + "," + lng;

        return staticMapUrl;
    }

    return {
        getMapPreviewSrc: getMapPreviewSrc
    };
})();
